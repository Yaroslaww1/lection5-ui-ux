### info ###

* [codepen link](https://codepen.io/yaroslaww1/pen/JjGmbOx)

### fixed problems ###

1. "Add request" button - remove border && hover && font color
2. "Import CSV" button - cursor && hover && remove border-radius && remove outline
3. "Export CSV" button - make the same as "Import CSV" button
4. footer - centered && space between date and copyright
5. header logo - centered
6. card status - padding
7. card - remove date shadow
8. card - add space between date and content
9. card - align date and content
10. card header - align date and header
11. card__header-meta - add space between calendar icon and users icon
12. card__header-meta - aligned items by center
13. add spaces between cards
14. card footer - always at bottom && change color
15. card specialties - padding left
16. home link && profile-img - add cursor: pointer
17. home link - change color && add hover 
18. card__header users icon - change color && add space between icon and number
19. booking-page__title - align with buttons text
20. filter input - add placeholder
21. filters - add filter button
22. filters - align filters and cards